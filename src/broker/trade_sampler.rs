
use crate::exchange::*;
use crate::prelude::*;

// es parecido a un Kline,
// acumulatorio de fuerzas
#[derive(Clone, Copy, Default, Debug, Serialize, Deserialize)]
pub struct TradeSample {
  //pub open_at: u64,
  // close at
  pub open_price: f64,
  pub close_price: f64,
  pub max_price: f64,
  pub min_price: f64,
  pub avg_price: f64,

  pub trades: f64,
  pub volume: f64,
}

impl TradeSample {
  pub fn to_trade_event(&self) -> TradeEvent {
    TradeEvent {
      trades: self.trades,
      volume: self.volume,
      price: self.avg_price,
    }
  }
  
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TradeSampler {
  is_empty: bool,
  close_price: f64,
  open_price: f64,
  max_price: f64,
  min_price: f64,

  acc_volume: f64,
  acc_price: f64,
  acc_trades: f64, // numero de trades

}

impl TradeSampler {
  pub fn new() -> TradeSampler {
    Default::default()
  }
}

impl Default for TradeSampler {
  fn default() -> Self { 
    TradeSampler { 
      is_empty: true,
      open_price: 0.0,
      close_price: 0.0,
      max_price: f64::NEG_INFINITY,
      min_price: f64::INFINITY,
      acc_price: 0.0,
      acc_trades: 0.0,
      acc_volume: 0.0,
    }
  }

}

impl Sampler<TradeEvent, TradeSample> for TradeSampler  {

  fn sample(&mut self, e: TradeEvent) { // tn
    
    if self.is_empty  {
      self.open_price = e.price;
      self.is_empty = false;
    }

    self.close_price = e.price;

    if e.price > self.max_price {
      self.max_price = e.price;
    } 
    if e.price < self.min_price {
      self.min_price = e.price
    }

    self.acc_trades += e.trades;
    self.acc_price += e.trades * e.price;
    self.acc_volume += e.volume;
  }

  fn emit(&mut self, _: Instant) -> Option<TradeSample> {

    if self.is_empty {
      return None
    }

    let sample = TradeSample {
      open_price: self.open_price,
      close_price: self.close_price,
      max_price: self.max_price,
      min_price: self.min_price,
      avg_price: if self.acc_trades > 0.0 {
        self.acc_price / self.acc_trades 
      } else { 
        0.5 * (self.open_price + self.close_price)  
      },
      trades: self.acc_trades,
      volume: self.acc_volume,
    };

    self.open_price = self.close_price;
    self.max_price = self.close_price;
    self.min_price = self.close_price;
    self.acc_price = 0.0;
    self.acc_trades = 0.0;
    self.acc_volume = 0.0;

    Some(sample)
  }
}
