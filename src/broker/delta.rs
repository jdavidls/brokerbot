use crate::prelude::*;

#[derive(Debug, Clone, PartialEq)]
pub enum Event {
  Nothing,
  //Ascending{ raise: f64, fall: f64 },
  //Descending{ fall: f64, raise: f64 },
  StopAscending(f64),
  StopDescending(f64),
  AscendingThreshold(f64),
  DescendingThreshold(f64),
}

pub use Event::*;

#[derive(Debug, Clone)]
pub struct Settings {
  ascending_threshold: f64,
  descending_threshold: f64,
}

#[derive(Debug, Clone, Default)]
struct State {
  last_value: Option<f64>,
  delta: f64,
  ascending_threshold: f64,
  descending_threshold: f64,
}

pub fn delta(input: impl Stream<Item = f64>) -> impl Stream<Item = Event> {
  Settings::new().delta(input)
}

//enum Direction

impl Default for Settings {
  fn default() -> Settings {
    Settings {
      ascending_threshold: 1.001,
      descending_threshold: 0.999,
    }
  }
}

impl Settings {
  pub fn new() -> Settings {
    Settings {
      ..Default::default()
    }
  }

  pub fn from_commission(c: f64, asymmetry: f64) -> Settings {
    Settings {
      ascending_threshold: 1.0 + c + c * asymmetry,
      descending_threshold: 1.0 - c + c * asymmetry,
    }
  }

  pub fn assymetric(&mut self, v: f64) -> &mut Self {
    self.ascending_threshold += v;
    self.descending_threshold += v;
    self
  }

  pub fn delta(&self, input: impl Stream<Item = f64>) -> impl Stream<Item = Event> {
    input
      .scan(State::new(self), |state, v| {
        future::ready(Some(state.step(v)))
      })
      .filter(|e| future::ready(*e != Nothing))
  }
}

impl State {
  pub fn new(settings: &Settings) -> State {
    State {
      ascending_threshold: settings.ascending_threshold,
      descending_threshold: settings.descending_threshold,
      delta: 1.0,
      ..Default::default()
    }
  }

  pub fn step(&mut self, value: f64) -> Event {
    let last_value = self.last_value.replace(value);

    if let Some(last_value) = last_value {

      let coeff = value / last_value;

      let last_delta = self.delta; // direccionalidad..

      // Si la direccion acumulada es hacia abajo
      if last_delta < 1.0 {

        // y el nuevo coeficiente es hacia arriba
        if coeff > 1.0 {
          // inicia el acumulador con el nuevo coeficiente
          self.delta = coeff;
          // retorna el evento que notifica que ha dejado de descender..
          return StopDescending(last_delta);
        }

        // en caso contrario introduce el nuevo coeficiente en el acumulador
        self.delta *= coeff;

        // comprueba si se cruza el umbral ascendente
        if last_delta > self.descending_threshold && self.delta < self.descending_threshold {
          // envia el delta acumulado
          return DescendingThreshold(self.delta);
        }
      }

      if last_delta > 1.0 {
        if coeff < 1.0 {
          self.delta = coeff;

          return StopAscending(last_delta);
        }

        self.delta *= coeff;

        if last_delta < self.ascending_threshold && self.delta > self.ascending_threshold {
          return AscendingThreshold(self.delta);
        }
      }

      if last_delta == 1.0 {
        self.delta *= coeff;
      }
    }

    Nothing
  }
}
