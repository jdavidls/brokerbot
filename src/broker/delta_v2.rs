use crate::prelude::*;



#[derive(Debug, Clone)]
pub struct Settings {
  ascending_threshold: f64,
  descending_threshold: f64,
}

pub fn delta(input: impl Stream<Item = f64>) -> impl Stream<Item = f64> {
  Settings::new().delta(input)
}

//enum Direction

impl Default for Settings {
  fn default() -> Settings {
    Settings {
      ascending_threshold: 0.001,
      descending_threshold: -0.001,
    }
  }
}

impl Settings {
  pub fn new() -> Settings {
    Settings {
      ..Default::default()
    }
  }
    
  pub fn delta(&self, input: impl Stream<Item = f64>) -> impl Stream<Item = f64> {
    input
    .scan(0.0, |a, b| {

      let delta = b / *a;

      *a = b;

      future::ready(Some(delta))
    })
    .skip(1) // descata el primer infinito.
  }

  pub fn delta_events(&self, input: impl Stream<Item = f64>) -> impl Stream<Item = f64> {
    self.delta(input)
      .scan(State::new(self), |state, v| {
        future::ready(Some(state.step(v)))
      })
      .filter(|e| future::ready(*e != 1.0))
  }
}


#[derive(Debug, Clone, Default)]
struct State {
  pre: f64,
  acc: f64,
  min: f64,
  max: f64,
  ascending_threshold: f64,
  descending_threshold: f64,
}


impl State {
  pub fn new(settings: &Settings) -> State {
    State {
      ascending_threshold: settings.ascending_threshold,
      descending_threshold: settings.descending_threshold,
      acc: 1.0,
      min: 1.0,
      max: 1.0,
      ..Default::default()
    }
  }

  fn reset(&mut self) {
    self.acc = 1.0;
    self.min = 1.0;
    self.max = 1.0;
  }

  pub fn step(&mut self, cur: f64) -> f64 {

    let acc = self.acc * cur;

    if acc < 1.0 { // en bajada...

      if self.min > acc {
        self.min = acc;
      }

      if acc - self.min >= self.ascending_threshold {
        self.reset();
        return acc;
      } 

    }

    if self.acc > 1.0 {  // en subida

      if self.max < acc {
        self.max = acc;
      }

      
      if acc - self.max <= self.descending_threshold {
        self.reset();
        return acc;
      }

    }

    self.acc = acc;

    1.0
  }
}
