// ab BTC(usd) ac ETH(usd)  ETH(BTC)
pub fn trinity_delta(ab: f64, ac: f64, bc: f64) -> (f64, f64, f64) {
  let a = -(ab + ac) / 2.0;
  let b = (ab - bc) / 2.0;
  let c = (ac - bc) / 2.0;

  (a, b, c)
}
