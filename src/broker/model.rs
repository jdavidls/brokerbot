use crate::prelude::*;
use super::TokenPair;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TradeEvent {
  pub at: u64,
  pub pair: TokenPair,
  pub price: f64,
  pub trades: f64,
  pub volume: f64,
}
/*
impl TradeEvent {
  pub fn new() {
  }
}
*/
#[derive(Clone, Copy, Default, Debug, Serialize, Deserialize)]
pub struct TradeSample {
  pub open_at: u64,
  pub close_at: u64,
  pub open_price: f64,
  pub close_price: f64,
  pub max_price: f64,
  pub min_price: f64,
  pub avg_price: f64,

  pub trades: f64,
  pub volume: f64,
}

impl TradeSample {
  pub fn duration(&self) -> u64 {
    self.close_at - self.open_at
  }

  pub fn mock_iter(&self, pair: TokenPair) -> TradeSampleMockIter {
    TradeSampleMockIter::new(pair, &self)
  }

}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TradeSamples {
  pub pair: TokenPair,
  pub samples : Vec<TradeSample>,
}

impl TradeSamples {
  pub fn mock_iter<'a>(&'a self) -> impl Iterator<Item=TradeEvent> + 'a {
    let pair = self.pair;
    self.samples.iter().flat_map(move |sample| {
      sample.mock_iter(pair)
    })
  }
}


#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Side {
    Buy,
    Sell,
}


pub struct TradeSampleMockIter<'a> {
  pair: TokenPair,
  sample: &'a TradeSample,
  count: usize,
}

impl<'a> TradeSampleMockIter<'a> {
  pub fn new(pair: TokenPair, sample: &'a TradeSample) ->  TradeSampleMockIter {
    TradeSampleMockIter { pair, sample, count: 0 }
  }
}

impl<'a> Iterator for TradeSampleMockIter<'a> {
  type Item = TradeEvent;

  fn next(&mut self) -> Option<TradeEvent> {

    let time_step = self.sample.duration() / 3;

    let (price, at) = match self.count {
      0 => (self.sample.open_price, self.sample.open_at),
      1 => (self.sample.max_price, self.sample.open_at + time_step),
      2 => (self.sample.min_price, self.sample.close_at - time_step),
      3 => (self.sample.close_price, self.sample.close_at),
      _ => return None,
    };

    self.count+=1;

    Some(TradeEvent {
      pair: self.pair,
      at, 
      price, 
      volume: self.sample.volume / 4.0,
      trades: self.sample.trades / 4.0,
    })
  }
}


#[cfg(test)]
mod test {

  #[test]
  pub fn mock_sampling_test() {

  }

}