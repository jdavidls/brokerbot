/*
Consumidor principal de los trade streams, hace broadcast de dichos eventos..
recibe (y monitoriza) ordenes de trading desde el bot,
*/
use crate::binance;
use crate::prelude::*;
use std::collections::HashMap;
use tokio::sync::broadcast::{channel, Receiver, Sender};
use tokio::task::JoinHandle;

const BROADCAST_BUFFER: usize = 65536;

#[derive(Debug, PartialEq, Eq)]
pub enum BrokerInternalError {
  BroadcastError,
  TradeStreamError,
}

impl std::fmt::Display for BrokerInternalError {
  fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(fmt, "Broker internal error")
  }
}

impl std::error::Error for BrokerInternalError {}

unsafe impl Send for BrokerInternalError {}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Settings {
  binance: binance::Settings,
  pairs: Vec<TokenPair>,
}

impl Default for Settings {
  fn default() -> Settings {
    Settings {
      pairs: vec![BTC / USDT, ETH / USDT, ETH / BTC],
      binance: Default::default(),
    }
  }
}

impl Settings {
  pub async fn connect(self) -> Result<Broker> {
    Broker::connect(self).await
  }
}

pub enum Event {
  Trade(TradeEvent),
  LiveMode,
  // OrderSuccess
  // OrderFailure
  // ModeChange(RealMode/MockMode)
}

#[derive(Default)]
struct MarketEntry {
  current_price: f64,
}

#[derive(Default, Debug)]
struct Balance {
  free: f64,
  locked: f64,
  mock: f64,
}

#[derive(Default, Debug)]
struct AssetEntry {
  balance: Balance,
}

pub trait Exchange {}

pub enum Notification {
  //TransactionComplete
}

/*
#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct TradeStream {
    #[pin]
    ws: JSonWebSocket<WSInputPacket<AggTradePacket>, ()>,
}
*/
#[pin_project]
pub struct Broker {
  binance: binance::BinanceClient,
  pairs: Vec<TokenPair>,
  #[pin]
  event_stream: stream::Fuse<BoxStream<'static, Result<Event>>>,
  
  //wallet: HashMap<Token, AssetEntry>,
  wallet : TokenMap<AssetEntry>,

  markets: HashMap<TokenPair, MarketEntry>,

  is_live_mode: bool,
  is_real_mode: bool,
}

impl Broker {
  pub async fn connect(settings: Settings) -> Result<Broker> {
    let Settings { pairs, binance } = settings;

    let binance = binance.client();

    let event_stream = Self::prepare_event_stream(&binance, &pairs)
      .await?
      .boxed()
      .fuse();

    let wallet = Self::prepare_wallet(&binance, &pairs).await?;
    let markets = Self::prepare_markets(&binance, &pairs).await?;

    //let (res_tx, res_rx) = channel(1);

    Ok(Broker {
      binance,
      pairs,
      event_stream,
      wallet,
      markets,
      is_live_mode: false,
      is_real_mode: false,
    })
  }

  pub fn is_live_mode(&self) -> bool {
    self.is_live_mode
  }

  pub fn is_real_mode(&self) -> bool {
    self.is_real_mode
  }


  pub async fn set_real_mode(&mut self) -> Result<()>  {
    assert!(self.is_live_mode() == true, "Real mode when not live mode enabled!!");
    self.is_real_mode = true;
    Ok(())
  }

  async fn prepare_event_stream(
    binance: &binance::BinanceClient,
    pairs: &[TokenPair],
  ) -> Result<impl Stream<Item = Result<Event>>> {
    let samples = pairs
      .iter()
      .map(|pair| binance.get_market_samples(*pair, "1m"));

    let all_samples = future::try_join_all(samples).await?;

    let mut trade_events: Vec<TradeEvent> = all_samples
      .iter()
      .flat_map(|samples| samples.mock_iter())
      .collect();

    trade_events.sort_by_key(|event| event.at);

    let ws_stream = stream::select_all(
      future::try_join_all(pairs.iter().map(|pair| binance.trade_stream(vec![*pair]))).await?,
    );

    Ok(
      stream::iter(trade_events)
        .map(|e| Ok(Event::Trade(e)))
        .chain(stream::once(async { Ok(Event::LiveMode)} ))
        .chain(ws_stream.map_ok(|trade| Event::Trade(trade))),
    )
  }

  async fn prepare_wallet(
    binance: &binance::BinanceClient,
    pairs: &[TokenPair],
  ) -> Result<TokenMap<AssetEntry>> {
    Ok(Default::default())
  }

  async fn prepare_markets(
    binance: &binance::BinanceClient,
    pairs: &[TokenPair],
  ) -> Result<HashMap<TokenPair, MarketEntry>> {
    let mut markets: HashMap<TokenPair, MarketEntry> = HashMap::new();

    for pair in pairs.iter() {
      markets.insert(
        *pair,
        MarketEntry {
          current_price: binance.get_price(*pair).await?,
        },
      );
    }

    Ok(markets)
  }

  pub fn token_pairs(&self) -> Vec<TokenPair> {
    self.pairs.clone()
  }

  pub async fn get_price(&self, pair: TokenPair) -> Result<f64> {
    Ok(self.markets.get(&pair).unwrap().current_price)
  }

  pub fn set_balance(&mut self, token: Token, amount: f64) {
    self.wallet.get_mut(token).balance.free += amount;
  }

  pub fn get_balance(&self, token: Token) -> f64 {
    self.wallet.get(token).balance.free
  }

  pub async fn market_order(
    &mut self,
    pair: TokenPair,
    side: Side,
    amount: f64,
  ) -> Result<(f64, f64)> {
    let market = self.markets.get(&pair).unwrap();
    let market_price = market.current_price;

    match side {
      Side::Buy => {

        if self.is_real_mode {
          let operative_balance = self.binance.get_balance(pair.from).await?.free * 0.98;
          let trx = self.binance.market(Side::Buy, pair, operative_balance * 0.98).await?;
          dbg!(trx);
        }

        let give_amount = self.wallet.get(pair.from).balance.free.min(amount);

        self.wallet.get_mut(pair.from).balance.free -= give_amount; // dejar la calderilla

        let penalty = give_amount * 0.001;
        let take_amount = (give_amount - penalty) / market_price;

        self.wallet.get_mut(pair.to).balance.free += take_amount;

        println!("BUY  {:.6} @ {:.6} {} = {:.6} {}", give_amount, market_price, pair.from, take_amount, pair.to);

        Ok((take_amount, penalty))
      },
      Side::Sell => {

        if self.is_real_mode {
          let operative_balance = self.binance.get_balance(pair.to).await?.free * 0.98;
          let trx = self.binance.market(Side::Sell, pair, operative_balance * market_price).await?;
          dbg!(trx);
        }

        let give_amount = self.wallet.get(pair.to).balance.free.min(amount);

        self.wallet.get_mut(pair.to).balance.free -= give_amount; // deja calderillaaa

        let penalty = give_amount * 0.001;
        let take_amount = (give_amount - penalty) * market_price;

        self.wallet.get_mut(pair.from).balance.free += take_amount;


        println!("SELL {:.6} @ {:.6} {} = {:.6} {}", give_amount, market_price, pair.from, take_amount, pair.from);

        Ok((take_amount, penalty))
      }
    }


  }

}

impl Stream for Broker {
  type Item = Result<Event>;

  #[project]
  fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
    // efectuar comprobaciones .. actualizar un precio de mercado puede
    // desencadenar en alguna consecuencia sobre una orden (limit, stop-limit etc..)

    #[project]
    let Broker {
      mut event_stream,
      mut markets,
      mut is_live_mode,
      ..
    } = self.project();
    Poll::Ready(match ready!(event_stream.as_mut().poll_next(cx)) {
      Some(Ok(Event::Trade(trade))) => {
        // update market_price
        if let Some(market) = markets.get_mut(&trade.pair) {
          market.current_price = trade.price;
        }

        // Puede causar

        Some(Ok(Event::Trade(trade)))
      },
      Some(Ok(Event::LiveMode)) =>       {
        *is_live_mode  = true;

        Some(Ok(Event::LiveMode))
      },
      Some(Ok(event)) => Some(Ok(event)),
      Some(Err(e)) => Some(Err(e)),
      None => None,
    })
  }
}

impl stream::FusedStream for Broker {
  fn is_terminated(&self) -> bool {
    self.event_stream.is_terminated()
  }
}
