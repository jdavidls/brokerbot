use serde::{Deserialize, Deserializer, Serialize, Serializer};


#[repr(u8)]
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq, PartialOrd, Serialize, Deserialize)]
pub enum Token {
  USDT = 0,
  BTC = 1,
  ETH = 2,
  ALGO = 3,
  COMP = 4,
  HIVE = 5,

  // siempre al finall
  XTZ = 6,
}

pub struct TokenMap<T> {
  map: [T; Token::XTZ as usize + 1]
}

impl<T> TokenMap<T> {
  pub fn get(&self, token: Token) -> &T{
    &self.map[token as usize]
  }

  pub fn get_mut(&mut self, token: Token) -> &mut T{
    &mut self.map[token as usize]
  }
}

impl<T> Default for TokenMap<T> 
where T: Default
{
  fn default() -> TokenMap<T> {
    TokenMap{
      map: Default::default()
    }
  }

}


impl std::ops::Div<Token> for Token {
  type Output = TokenPair;

  fn div(self, rhs: Token) -> Self::Output {
    TokenPair {
      to: self,
      from: rhs,
    }
  }
}

pub use Token::*;

impl std::cmp::Ord for Token {
  fn cmp(&self, rhs: &Self) -> std::cmp::Ordering {
    let lhs = unsafe { *(self as *const Token as *const u8) };
    let rhs = unsafe { *(rhs as *const Token as *const u8) };
    lhs.cmp(&rhs)
  }
}

impl Token {
  fn to_str(&self) -> &str {
    match self {
      ALGO => "ALGO",
      BTC => "BTC",
      ETH => "ETH",
      USDT => "USDT",
      COMP => "COMP",
      XTZ => "XTZ",
      HIVE => "HIVE",
    }
  }
}


impl std::fmt::Display for Token {
  fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    fmt.write_str(self.to_str())
  }
}

impl std::str::FromStr for Token {
  type Err = ParseTokenError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    match s {
      "ALGO" => Ok(ALGO),
      "BTC" => Ok(BTC),
      "ETH" => Ok(ETH),
      "USDT" => Ok(USDT),
      "COMP" => Ok(COMP),
      "HIVE" => Ok(HIVE),
      _ => Err(ParseTokenError(s.into())),
    }
  }
}



#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct TokenPair {
  pub to: Token,
  pub from: Token,
}



impl TokenPair {
  pub const fn new(to: Token, from: Token) -> TokenPair {
    TokenPair { to, from }
  }
}


impl std::str::FromStr for TokenPair {
  type Err = ParseTokenError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    match s {
      "ALGOBTC" => Ok(ALGO / BTC),
      "ALGOUSDT" => Ok(ALGO / USDT),
      
      "USDTBTC" => Ok(USDT / BTC),
      "USDTETH" => Ok(USDT / ETH),

      "BTCUSDT" => Ok(BTC / USDT),
      "BTCETH" => Ok(BTC / ETH),
      "BTCCOMP" => Ok(BTC / COMP),

      "ETHUSDT" => Ok(ETH / USDT),
      "ETHBTC" => Ok(ETH / BTC),

      "HIVEBTC" => Ok(HIVE / BTC),
      "HIVEUSDT" => Ok(HIVE / USDT),

      "XTZBTC" => Ok(XTZ / BTC),
      "XTZUSDT" => Ok(XTZ / USDT),

      "COMPBTC" => Ok(COMP / BTC),
      "COMPUSDT" => Ok(COMP / USDT),
      _ => Err(ParseTokenError(s.into())),
    }
  }
}

impl std::fmt::Display for TokenPair {
  fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    self.to.fmt(fmt)?;
    self.from.fmt(fmt)
  }
}

mod test {
  use super::*;

  #[test]
  fn parse_token() {
    //let token: super::Token = ;
    assert_eq!(BTC, "BTC".parse().expect("\"BTC\" not parsed"));
    let unknown_token: Result<Token, ParseTokenError> = "ZZZ".parse();

    assert_eq!(
      ParseTokenError("ZZZ".into()),
      unknown_token.expect_err("Unknown token has been parsed")
    );
  }

  #[test]
  fn parse_token_pair() {
    //let token: super::Token = ;
    assert_eq!(BTC_USDT, "BTCUSDT".parse().expect("\"BTCUSDT\" not parsed"));
    //let unknown_token: Result<Token, ParseTokenError> = "ZZZ".parse();

    //assert_eq!(ParseTokenError("ZZZ".into()), unknown_token.expect_err("Unknown token has been parsed"));
  }
}




#[derive(Debug, PartialEq, Eq)]
pub struct ParseTokenError(String);


impl std::fmt::Display for ParseTokenError {
  fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(fmt, "Error parsing token pair: unknown pair {}", self.0)
  }
}

impl std::error::Error for ParseTokenError {}
