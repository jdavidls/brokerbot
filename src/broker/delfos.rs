const INITIAL_CAPACITY: usize = 65536;

pub fn delfos(threshold: f64, probe_max_lifetime: usize) -> Delfos {
  Delfos {
    last_value: 1.0,
    probes: Vec::with_capacity(INITIAL_CAPACITY),
    threshold,
    probe_max_lifetime,
  }
}

#[derive(Debug)]
pub struct Delfos {
  last_value: f64,
  probes: Vec<Probe>,
  probe_max_lifetime: usize,
  threshold: f64,
}

#[derive(Debug)]
struct Probe {
  lifetime: usize,
  acc: f64,
  cross_sell: bool,
  cross_buy: bool,
  out_of_threshold: bool,
}

impl Probe {
  pub fn new() -> Probe {
    Probe {
      lifetime: 0,
      acc: 1.0,
      cross_sell: false,
      cross_buy: false,
      out_of_threshold: true,
    }
  }
}

impl Delfos {

  pub fn signal(&mut self, delta: f64) -> (f64, f64, f64) {
      self.launches_new_probe();
      let result = self.process_delta(delta);
      self.kill_probes();
      result
  }

  fn launches_new_probe(&mut self) {
    self
      .probes
      .push(Probe::new())
  }

  pub fn living_probes(&self) -> usize {
    self.probes.len()
  }

  fn process_delta(&mut self, delta: f64) -> (f64, f64, f64) {
    let buy_threshold = self.threshold;
    let sell_threshold = 1.0/self.threshold;

    let mut buy = 1.0_f64;
    let mut buy_count = 1.0_f64;

    let mut sell = 1.0_f64;
    let mut sell_count = 1.0_f64;

    let mut doubt = self.probes.len();

    for mut probe in self.probes.iter_mut() {
      probe.acc *= delta;

      if probe.acc < buy_threshold && !probe.cross_sell {
        buy += 1.0 / (probe.acc * self.threshold);
        buy_count += 1.0;
        probe.cross_buy = true;
        probe.out_of_threshold = false;
        doubt -= 1;

      } else if probe.acc > sell_threshold && !probe.cross_buy {
        sell += probe.acc * self.threshold;
        sell_count += 1.0;
        probe.cross_sell = true;
        probe.out_of_threshold = false;
        doubt -= 1;        

      } else {
        probe.out_of_threshold = true;
      }

      probe.lifetime += 1;
    }

    let doubt = doubt as f64 / self.probes.len() as f64;

    (buy / buy_count, sell / sell_count, doubt)
  }


  pub fn clear(&mut self) {
    self.probes.clear();
    self.launches_new_probe();
  }

  fn kill_probes(&mut self) {
    let probe_max_lifetime = self.probe_max_lifetime;

    self.probes = self
      .probes
      .drain(..)
      .filter(|probe| {
        if probe.out_of_threshold {
          if probe.cross_buy || probe.cross_sell {
            return false;
          }
        }

        if probe.lifetime > probe_max_lifetime {
          return false;
        }

        return true;
      })
      .collect();
  }
}
