use super::trade_sampler::TradeSample;
use crate::prelude::*;
/*
Delta toma un TradeStream y lo mapea realizando una comparacion entre t y t-1

*/

#[derive(Clone, Debug, Default)]
pub struct DeltaSample {
  //min_price: f64,
  pub avg_price: f64,
  //acc_delta
}

pub fn delta(input: impl Stream<Item = TradeSample>) -> impl Stream<Item = DeltaSample> {

  let init : TradeSample = Default::default();

  input
    .scan(init, |tm, tn| {
      let result = DeltaSample {
        //min_price: __delta(tm.min_price, tn.min_price),
        //max_price: __delta(tm.max_price, tn.max_price),
        avg_price: __delta(tm.avg_price, tn.avg_price),
      };
      *tm = tn;
      future::ready(Some(result))
    })
    .skip(1)
}

fn __delta(a: f64, b: f64) -> f64 {
  if a < b {
    (b / a) - 1.0
  } else {
    -((a / b) - 1.0)
  }
}
