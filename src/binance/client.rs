use binance;
use binance::api::Binance as _;

use super::inner;

use crate::prelude::*;

use bytes::buf::{Buf as _, BufExt as _};
use serde::{de::DeserializeOwned, Deserialize, Serialize};

use crate::broker::{Token, TokenPair, TradeEvent};

use crate::utils::{
    http, string_or_float,
    ws::{JSonWebSocket, WebSocket},
};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Settings {
    pub api_rest_url: String,
    pub api_ws_url: String,

    pub api_key: Option<String>,
    pub api_secret: Option<String>,
}

impl Default for Settings {
    fn default() -> Settings {
        Settings {
            api_rest_url: "https://api.binance.com".to_owned(),
            api_ws_url: "wss://stream.binance.com:9443".to_owned(),
            api_key: None,
            api_secret: None,
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct GetPriceResponse {
    pub symbol: String,

    #[serde(with = "string_or_float")]
    pub price: f64,
}

#[derive(Deserialize, Debug)]
pub struct WSInputPacket<T> {
    pub stream: String,
    pub data: T,
}


#[derive(Deserialize, Debug)]
pub struct WSOutputPacket {}
/*
#[derive(Deserialize, Debug)]
#[serde(tag = "e")]
pub enum WSStreamInput {
    #[serde(rename = "aggTrade")]
    AggTrade(AggTradePacket),
}
*/
#[derive(Default, Debug)]
pub struct Balance {
  pub free: f64,
  pub locked: f64,
}


#[derive(Deserialize, Debug)]
pub struct AggTradePacket {
    #[serde(rename = "E")]
    pub at: u64,

    #[serde(rename = "s")]
    pub pair: String,

    #[serde(rename = "a")]
    pub trade_id: u64,

    #[serde(rename = "p", with = "string_or_float")]
    pub price: f64,

    #[serde(rename = "q", with = "string_or_float")]
    pub qty: f64,

    #[serde(rename = "f")]
    pub first_trade_id: u64,

    #[serde(rename = "l")]
    pub last_trade_id: u64,

    #[serde(rename = "T")]
    pub trade_at: u64,
}

impl Settings {
    pub fn client(&self) -> BinanceClient {
        BinanceClient::new(self.clone())
    }
}

#[derive(Clone)]
pub struct BinanceClient {
    pub(crate) settings: Settings,

    http: http::Client,
    //
    account: binance::account::Account,
    market: binance::market::Market,
}

impl BinanceClient {
    fn new(settings: Settings) -> BinanceClient {
        BinanceClient {
            settings: settings.clone(),
            http: http::new_client(),
            account: binance::account::Account::new(
                settings.api_key.clone(),
                settings.api_secret.clone(),
            ),
            market: binance::market::Market::new(
                settings.api_key.clone(),
                settings.api_secret.clone(),
            ),
        }
    }

    pub(crate) async fn get<T, QS>(&self, endpoint: &str, qs: Option<QS>) -> Result<T>
    where
        QS: Serialize,
        T: DeserializeOwned,
    {
        let qs = serde_qs::to_string(&qs)?;

        let uri = self.settings.api_rest_url.clone();

        let req = http::Request::get(uri + endpoint + "?" + &qs[..])
            .header("User-Agent", "BROKER_BOT 0.1")
            .header("X-MBX-APIKEY", self.settings.api_key.clone().unwrap())
            .body(http::Body::empty())?;

        let res = self.http.request(req).await?;

        let body = hyper::body::aggregate(res).await?;

        serde_json::from_reader(body.reader()).map_err(|e| e.into())
    }

    pub async fn trade_stream(&self, pairs: Vec<TokenPair>) -> Result<TradeStream> {
        let streams = pairs
            .iter()
            .map(|pair| format!("{}@aggTrade", pair.to_string().to_lowercase()))
            .collect::<Vec<String>>()
            .join("/");

        let uri = format!("{}/stream?streams={}", self.settings.api_ws_url, streams);

        let ws = WebSocket::connect(&uri).await?;

        Ok(TradeStream { ws: ws.json() })
    }

    pub async fn get_market_samples(
        &self,
        pair: TokenPair,
        interval: &str,
    ) -> Result<TradeSamples> {
        //let req
        let mut result: Vec<inner::GetCandlestickResItem> = self
            .get(
                "/api/v3/klines",
                Some(inner::GetCandlestickReq {
                    symbol: pair.to_string(),
                    interval: interval.to_owned(),
                    limit: Some(1000),
                    ..Default::default()
                }),
            )
            .await?;

        Ok(TradeSamples {
            pair,
            samples: result.drain(..).map(|v| v.into()).collect(),
        })
    }

    pub async fn get_balance(&self, token: Token) -> Result<Balance> {
        let account = self.account.clone();

        let binance::model::Balance { free, locked, .. } =
            tokio::task::spawn_blocking(move || -> binance::model::Balance {
                account
                    .get_balance(token.to_string())
                    .expect("Internal api error")
            })
            .await?;

        Ok(Balance {
            free: free.parse()?,
            locked: locked.parse()?,
        })
    }

    pub async fn market(
        &self,
        side: Side,
        pair: TokenPair,
        qty: f64,
    ) -> Result<binance::model::Transaction> {
        let account = self.account.clone();

        Ok(
            tokio::task::spawn_blocking(move || -> binance::model::Transaction {
                match side {
                    Side::Buy => account.market_buy(pair.to_string(), qty),
                    Side::Sell => account.market_sell(pair.to_string(), qty),
                }
                .expect("API ERROR")
            })
            .await
            .expect("API ERROR")
        )
    }

    pub async fn market_test(&self, side: Side, pair: TokenPair, qty: f64) -> Result<()> {
        let account = self.account.clone();

        tokio::task::spawn_blocking(move || {
            match side {
                Side::Buy => account.test_market_buy(pair.to_string(), qty),
                Side::Sell => account.test_market_sell(pair.to_string(), qty),
            }
            .expect("Internal api error")
        })
        .await
        .expect("ERROR");

        Ok(())
    }

    pub async fn get_price(&self, pair: TokenPair) -> Result<f64> {
        let market = self.market.clone();

        let price = tokio::task::spawn_blocking(move || -> binance::model::SymbolPrice {
            market
                .get_price(pair.to_string())
                .expect("Internal api error")
        })
        .await?;

        Ok(price.price)
    }
}

#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct TradeStream {
    #[pin]
    ws: JSonWebSocket<WSInputPacket<AggTradePacket>, ()>,
}
/*
unsafe impl Send for TradeStream {

}
*/
impl Stream for TradeStream {
    type Item = Result<TradeEvent>;

    #[project]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        #[project]
        let TradeStream { mut ws, .. } = self.project();

        Poll::Ready(match ready!(ws.as_mut().poll_next(cx)) {
            Some(Ok(packet)) => Some(Ok(TradeEvent {
                at: packet.data.at,
                pair: packet.data.pair.parse()?,
                trades: packet.data.last_trade_id as f64 - packet.data.first_trade_id as f64,
                price: packet.data.price,
                volume: packet.data.qty,
            })),
            Some(Err(e)) => Some(Err(e)),
            None => None,
        })
    }
}
