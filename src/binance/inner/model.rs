use crate::prelude::*;

use crate::broker::TradeSample;
use crate::utils::string_or_float;

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum OrderStatus {
  New,
  PartiallyFilled,
  Filled,
  Canceled,
  PendingCancel,
  Rejected,
  Expired,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum OrderType {
  Market,
  Limit,
  StopLoss,
  StopLossLimit,
  TakeProfit,
  TakeProfitLimit,
  LimitMaker,
}


#[derive(Default, Serialize, Deserialize, Clone, Debug)]
pub struct GetCandlestickReq {
  pub symbol: String,
  pub interval: String,
  pub start_time: Option<u64>,
  pub end_time: Option<u64>,
  pub limit: Option<u64>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct GetCandlestickResItem(
  /*0 open_time: */ u64,
  
  #[serde(with = "string_or_float")]
  /*1 open_price: */ f64,
  
  #[serde(with = "string_or_float")]
  /*2 max_price: */ f64,
  
  #[serde(with = "string_or_float")]
  /*3 min_price: */ f64,
  
  #[serde(with = "string_or_float")]
  /*4 close_price: */ f64,
  
  #[serde(with = "string_or_float")]
  /*5 volume: */ f64,

  /*6 close_time: */ u64,

  #[serde(with = "string_or_float")]
  /*7 quote_assets_volume: */ f64,
  
  /*8 trades: */ u64,
  
  #[serde(with = "string_or_float")]
  /*9 base_asset_volume: */ f64,
  
  #[serde(with = "string_or_float")]
  /*10 quote_asset_volume: */ f64,
  
  #[serde(with = "string_or_float")]
  /*11 ignore: */ f64,
);


impl From<GetCandlestickResItem> for TradeSample {

  fn from(from: GetCandlestickResItem) -> Self {
    TradeSample {
      open_at: from.0,
      close_at: from.6,
      open_price: from.1,
      max_price: from.2,
      min_price: from.3,
      close_price: from.4,
      avg_price: (from.2 + from.3) * 0.5,
      volume: from.5,
      trades: from.8 as f64,
    }
  }

}
