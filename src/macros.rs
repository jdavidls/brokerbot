
macro_rules! delegate_stream {
    ($field:ident) => {
        fn poll_next(
            self: core::pin::Pin<&mut Self>,
            cx: &mut futures::task::Context<'_>,
        ) -> futures::task::Poll<Option<Self::Item>> {
            self.project().$field.poll_next(cx)
        }
        fn size_hint(&self) -> (usize, Option<usize>) {
            self.$field.size_hint()
        }
    }
}

// https://docs.rs/crate/futures-util/0.3.5/source/src/lib.rs
macro_rules! delegate_sink_ready {
  ($field:ident, $item:ty) => {
      fn poll_ready(
          self: core::pin::Pin<&mut Self>,
          cx: &mut futures::task::Context<'_>,
      ) -> futures::task::Poll<std::result::Result<(), Self::Error>> {
          self.project().$field.poll_ready(cx).map_err(|e| e.into())
      }
  }
}

macro_rules! noop_sink_ready {
    ($field:ident, $item:ty) => {
        fn poll_ready(
            self: core::pin::Pin<&mut Self>,
            cx: &mut futures::task::Context<'_>,
        ) -> futures::task::Poll<std::result::Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
    }
  }
  

// https://docs.rs/crate/futures-util/0.3.5/source/src/lib.rs
macro_rules! delegate_sink_send {
  ($field:ident, $item:ty) => {

      fn start_send(
          self: core::pin::Pin<&mut Self>,
          item: $item,
      ) -> std::result::Result<(), Self::Error> {
          self.project().$field.start_send(item).map_err(|e| e.into())
      }

  }
}

// https://docs.rs/crate/futures-util/0.3.5/source/src/lib.rs
macro_rules! delegate_sink_flush {
  ($field:ident, $item:ty) => {

      fn poll_flush(
          self: core::pin::Pin<&mut Self>,
          cx: &mut futures::task::Context<'_>,
      ) -> futures::task::Poll<std::result::Result<(), Self::Error>> {
          self.project().$field.poll_flush(cx).map_err(|e| e.into())
      }
  }
}

// https://docs.rs/crate/futures-util/0.3.5/source/src/lib.rs
macro_rules! noop_sink_flush {
    ($field:ident, $item:ty) => {
          fn poll_flush(
            self: core::pin::Pin<&mut Self>,
            cx: &mut futures::task::Context<'_>,
        ) -> futures::task::Poll<std::result::Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
    }
  }
  

// https://docs.rs/crate/futures-util/0.3.5/source/src/lib.rs
macro_rules! delegate_sink_close {
  ($field:ident, $item:ty) => {

      fn poll_close(
          self: core::pin::Pin<&mut Self>,
          cx: &mut futures::task::Context<'_>,
      ) -> futures::task::Poll<std::result::Result<(), Self::Error>> {
          self.project().$field.poll_close(cx).map_err(|e| e.into())
      }
  }
}


// https://docs.rs/crate/futures-util/0.3.5/source/src/lib.rs
macro_rules! noop_sink_close {
    ($field:ident, $item:ty) => {
          fn poll_close(
            self: core::pin::Pin<&mut Self>,
            cx: &mut futures::task::Context<'_>,
        ) -> futures::task::Poll<std::result::Result<(), Self::Error>> {
            Poll::Ready(Ok(()))
        }
    }
  }
  
  