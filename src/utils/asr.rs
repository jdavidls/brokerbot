pub fn asr(attack_threshold: f64, release_threshold: f64) -> ASR {
  ASR {
    attack_threshold,
    release_threshold,
    capturing: false,
    max_level: 0.0,
    //dyn_release: 0.0,
  }
}

pub fn dyn_asr(attack_threshold: f64, release_threshold: f64) -> ASR {
  ASR {
    attack_threshold,
    release_threshold,
    capturing: false,
    max_level: 0.0,
    //dyn_release: 0.0,
  }
}

pub struct ASR {
  capturing: bool,
  max_level: f64,
  attack_threshold: f64,
  release_threshold: f64,
}

impl ASR {
  pub fn signal(&mut self, level: f64) -> Option<f64> {

    if self.capturing {
      if level > level {
        self.max_level = level;
      }

      if level < self.max_level - self.release_threshold {
        self.clear();
        return Some(self.max_level);
      }
    }
    else {
      if level > self.attack_threshold {
        self.capturing = true;
        self.max_level = level;
      }
    }

    None
  }

  pub fn recall(&mut self, attack_threshold: f64, release_threshold: f64) {

  }

  pub fn clear(& mut self)  {
    self.capturing = false;
    self.max_level = 0.0;
  }
}
