pub type Connector = hyper_tls::HttpsConnector<hyper::client::HttpConnector>;

pub use http::{Uri, uri, Request, Response};
pub use hyper::{Body};

use tokio::net::TcpStream;
//use tokio_tungstenite::WebSocketStream;


pub type Req = Request<Body>;
pub type Res = Response<Body>;
pub type Client = hyper::Client<Connector, Body>;

pub type WSMessage = tokio_tungstenite::tungstenite::protocol::Message;
pub type WSStream = tokio_tungstenite::WebSocketStream<
  tokio_tungstenite::stream::Stream<
    tokio::net::TcpStream, 
    tokio_tls::TlsStream<tokio::net::TcpStream>
  >
>;


pub fn new_client() -> Client {
  hyper::Client::builder().build::<_, Body>(Connector::new())
}


