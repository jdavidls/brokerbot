use crate::prelude::*;

pub fn tri_merge<T>(
  a: impl Stream<Item = T>,
  b: impl Stream<Item = T>,
  c: impl Stream<Item = T>,
) -> impl Stream<Item = (T, T, T)> {
  a.zip(b).zip(c).map(|((a, b), c)| (a, b, c))
}
