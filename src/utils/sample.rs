use crate::prelude::*;

pub trait Sampler<In, Out> {
  fn sample(&mut self, value: In);
  fn emit(&mut self, tick: Instant) -> Option<Out>;
}

pub fn sample<In, Out, Clk, InStream, Samp>(
  mut clk: Clk,
  mut input: InStream,
  mut sampler: Samp,
) -> Sample<Out>
where
  Clk: Stream<Item = Instant> + Send + Unpin + 'static,
  InStream: Stream<Item = Result<In>> + Send + Unpin + 'static,
  Samp: Sampler<In, Out> + Send + Unpin + 'static,
  In: Clone,
  Out: Clone  + Sync + Send + 'static,
{
  let (tx, rx) = subject::channel();

  let handle = spawn(async move {
    loop {
      select! {
        Some(Ok(trade)) = input.next() => {
          sampler.sample(trade);
        },
        Some(tick) = clk.next() => {
          if let Some(sample) = sampler.emit(tick) {
            tx.broadcast(sample).expect("ERROR");
          }
        },
        else => break
      }
    }
  });

  Sample { rx, handle }
}

#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct Sample<T> {
  #[pin]
  rx: subject::Receiver<T>,
  handle: tokio::task::JoinHandle<()>,
}

impl<T> Sample<T> {
  pub fn out(&self) -> subject::Receiver<T>
  where
    T: Clone,
  {
    self.rx.clone()
  }
}

impl<T> Stream for Sample<T>
where
  T: Clone,
{
  type Item = T;

  delegate_stream!(rx);
}
