use crate::prelude::*;
use tokio::sync::watch;

#[derive(Clone, Debug)]
enum State<T> {
  Empty,
  Value(T),
}


pub fn channel<T>() -> (subject::Sender<T>, subject::Receiver<T>)
where
  T: Clone,
{
  let (tx, rx) = watch::channel(State::Empty);

  (subject::Sender::new(tx), subject::Receiver::new(rx))
}

#[derive(Clone, Debug)]
pub struct SendError {}

impl std::fmt::Display for SendError {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "subject channel closed")
  }
}

impl std::error::Error for SendError {}

//#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct Sender<T> {
  //#[pin]
  tx: watch::Sender<State<T>>,
}

impl<T> Sender<T> {
  fn new(tx: watch::Sender<State<T>>) -> Sender<T> {
    Sender { tx }
  }

  pub fn broadcast(&self, item: T) -> Result<()> {
    self
      .tx
      .broadcast(State::Value(item))
      .map_err(|e| (SendError {}).into())
  }
/*
  pub fn close(&self) {
    self.tx.close()
  }
*/  
}

impl<T> Sink<T> for Sender<T>
where
  T: Clone,
{
  type Error = Error;

  fn start_send(self: Pin<&mut Self>, item: T) -> std::result::Result<(), Error> {
    self.broadcast(item)
  }

  noop_sink_ready!(tx, T);
  noop_sink_flush!(tx, T);
  noop_sink_close!(tx, T);
}

#[pin_project]
#[must_use = "streams do nothing unless polled"]
#[derive(Clone)]
pub struct Receiver<T> {
  #[pin]
  rx: watch::Receiver<State<T>>,
}

impl<T> Receiver<T> {
  fn new(rx: watch::Receiver<State<T>>) -> Receiver<T> {
    Receiver { rx }
  }
}

impl<T> Stream for Receiver<T>
where
  T: Clone,
{
  type Item = T;

  #[project]
  fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
    #[project]
    let Receiver { mut rx, .. } = self.project();

    Poll::Ready(loop {
      match ready!(rx.as_mut().poll_next(cx)) {
        Some(State::Value(v)) => break Some(v),
        Some(State::Empty) => continue,
        None => break None,
      }
    })
  }
}
