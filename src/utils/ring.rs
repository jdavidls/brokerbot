pub fn ring(len: usize) -> Ring {
  Ring {
    buffer: std::collections::VecDeque::with_capacity(len + 1),
  }
}

pub struct Ring {
  pub buffer: std::collections::VecDeque<f64>,
}

impl Ring {

  pub fn fill(mut self, value: f64) -> Self{
    while self.buffer.len() < self.buffer.capacity() {
      self.buffer.push_front(value);
    }

    self
  }

  pub fn insert(&mut self, value: f64) {
    if self.buffer.len() == self.buffer.capacity() {
      self.buffer.pop_back();
    }

    self.buffer.push_front(value);
  }

  pub fn mul(&self, len: usize) -> f64 {
    assert!(len < self.buffer.capacity(), "ring max len");
    self.buffer.iter().take(len).fold(1.0, |mul, x| mul * x)
  }

  pub fn sum(&self, len: usize) -> f64 {
    assert!(len < self.buffer.capacity(), "ring max len");
    self.buffer.iter().take(len).fold(0.0, |acc, x| acc + x)
  }

  pub fn avg(&self, len: usize) -> f64 {
    self.sum(len) / len.min(self.buffer.len()) as f64
  }

  pub fn last(&self) -> Option<&f64> {
    self.buffer.front()
  }

  pub fn at(&self, idx: usize) -> f64 {
    assert!(idx < self.buffer.capacity(), "ring max len");
    return self.buffer[idx];
  }


  pub fn conv(&self) -> f64 {
    //assert!(len < self.buffer.capacity(), "ring max len");
    self.buffer.iter().fold((1.0, 1.0), |mut state, v| {
      state.0 *= v;
      state.1 += state.0;
      state
    }).1 / self.buffer.len() as f64
  }
}
