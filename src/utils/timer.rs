use crate::prelude::*;

pub fn timer(duration: Duration) -> Timer {

  let mut timer = interval(duration);

  let (mut tx, rx) = subject::channel();

  let handle = spawn(async move {
      loop {
          tx.broadcast(timer.tick().await).expect("Timer error");
      }
  });

  Timer {
      rx,
      handle,
    }
}

#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct Timer {
  #[pin] rx: subject::Receiver<Instant>,
  handle: tokio::task::JoinHandle<()>
}

impl Timer {
  pub fn out(&self) -> subject::Receiver<Instant> {
    self.rx.clone()
  }
}

impl Stream for Timer {
  type Item = Instant;

  delegate_stream!(rx);
}
