use crate::prelude::*;
//use crate::macros::*;

use std::{
  pin::Pin, 
  task::{Context, Poll}
};

use tokio::{
  net::TcpStream
};

use pin_project::{pin_project, project};

use tokio_tls::TlsStream;

use tokio_tungstenite::{
  tungstenite::{
    protocol::Message, 
    //handshake::client::Response 
  }
};

use serde::{de::DeserializeOwned, Serializer};

type InnerStream = tokio_tungstenite::WebSocketStream<
  tokio_tungstenite::stream::Stream<
    TcpStream, 
    TlsStream<TcpStream>
  >
>;


#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct WebSocket { // WebSocketStream
  #[pin]  stream: InnerStream,
  pingpong: Option<Vec<u8>>,
  need_flush: bool,
}


impl WebSocket {
  pub async fn connect(uri: &str) -> Result<WebSocket> {

    let (stream, _) = tokio_tungstenite::connect_async(uri).await?;

    Ok(WebSocket { 
      stream,
      //rx, tx, 
      pingpong: None,
      need_flush: false,
      //response, 
    })
  }

  pub fn json<I,O>(self) -> JSonWebSocket<I,O> {
    JSonWebSocket{ ws: self, __: std::marker::PhantomData }
  }
}

impl Stream for WebSocket {
  type Item = Result<Message>;

  #[project]
  fn poll_next(
      self: Pin<&mut Self>, 
      cx: &mut Context<'_>
  ) -> Poll<Option<Self::Item>> {

    #[project]
    let WebSocket { mut stream, pingpong, need_flush, .. } = self.project();

    Poll::Ready(loop {
      if pingpong.is_some() {
        if let Err(e) = ready!(stream.as_mut().poll_ready(cx)) {
          break Some(Err(Box::new(e)));
        }

        if let Some(payload) = pingpong.take() {
          if let Err(e) = stream.as_mut().start_send(Message::Pong(payload)) {
            break Some(Err(Box::new(e)));
          }
        }

        *need_flush = true;
      }

      if *need_flush {
        if let Err(e) = ready!(stream.as_mut().poll_flush(cx)) {
          break Some(Err(Box::new(e)));
        }
      }

      break match ready!(stream.as_mut().poll_next(cx))  {
        Some(Ok(Message::Ping(payload))) => {
          pingpong.replace(payload);
          continue;
        },
        Some(Ok(msg @ _)) => Some(Ok(msg)),
        Some(Err(e)) => Some(Err(Box::new(e))),
        None => None
      }

    })
  }

}

impl Sink<Message> for WebSocket {
  type Error = Error;

  delegate_sink_ready!(stream, Message);
  delegate_sink_send!(stream, Message);
  delegate_sink_flush!(stream, Message);
  delegate_sink_close!(stream, Message);

}

#[pin_project]
pub struct JSonWebSocket<I,O> {
  #[pin] ws: WebSocket,
  __: std::marker::PhantomData<(I,O)>
}

impl<I,O> Stream for JSonWebSocket<I,O> 
where
  I: DeserializeOwned
{
  type Item = Result<I>;

  #[project]
  fn poll_next(
      self: Pin<&mut Self>, 
      cx: &mut Context<'_>
  ) -> Poll<Option<Self::Item>> {

    #[project]
    let JSonWebSocket { mut ws, .. } = self.project();

    Poll::Ready(loop {
      break match ready!(ws.as_mut().as_mut().poll_next(cx)) {
        Some(Ok(Message::Text(ref msg))) => {
          Some(serde_json::from_str(msg).map_err(|e| e.into()))
        },
        Some(Ok(..)) => continue, // Ignore messages Binary / Pong 
        Some(Err(e)) => Some(Err(e)),
        None => None
      }
    })
  }
}



impl<I,O> Sink<O> for JSonWebSocket<I,O>
where
  O: Serialize
{
  type Error = Error;

  fn start_send(
    self: Pin<&mut Self>,
    item: O,
  ) -> std::result::Result<(), Self::Error> {
    let item = serde_json::to_string(&item)?;
    self.project().ws.start_send(Message::Text(item)).map_err(|e| e.into())
  }

  delegate_sink_ready!(ws, O);
  delegate_sink_flush!(ws, O);
  delegate_sink_close!(ws, O);
}





#[cfg(test)]
mod test {
  use crate::prelude::*;
  use super::{WebSocket, Message};
  //use tokio::stream::StreamExt;
  //use futures_util::sink::SinkExt;

  #[tokio::test]
  async fn test_raw_websocket() -> Result<()> {
    let mut ws = WebSocket::connect("wss://echo.websocket.org").await?;
    let msg = "RUST WS ROCKS!!".to_string();
  
    ws.send(Message::Text(msg.clone())).await?;
  
    match ws.next().await {
      Some(Ok(Message::Text(rmsg))) => assert_eq!(rmsg, msg),
      _ => panic!("Bad response")
    }
    Ok(())
  }
  
  #[tokio::test]
  async fn test_json_websocket() -> Result<()> {
    let mut ws = WebSocket::connect("wss://echo.websocket.org").await?.json::<String, String>();
    let msg = "RUST WS ROCKS!!".to_string();
  
    ws.send(msg.clone()).await?;
  
    match ws.next().await {
      Some(Ok(rmsg)) => assert_eq!(rmsg, msg),
      _ => panic!("Bad response")
    }
    Ok(())
  }

}
