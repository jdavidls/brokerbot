pub fn delta() -> delta::State {
  delta::State { acc: 1.0 }
}

pub mod delta {

  pub struct State {
    pub acc: f64,
  }

  impl Default for State {
    fn default() -> State {
      State { acc: 1.0 }
    }
  }

  impl State {
    pub fn acc(&mut self, value: f64) {
      self.acc *= value;
    }

    pub fn reduce(&mut self) -> f64 {
      let acc = self.acc;
      self.acc = 1.0;
      return acc;
    }
  }
}

pub fn cma(len: usize) -> cma::State {
  cma::State {
    ring_buffer: std::collections::VecDeque::with_capacity(len),
  }
}

pub mod cma {

  pub struct State {
    pub ring_buffer: std::collections::VecDeque<f64>,
  }

  impl State {
    pub fn acc(&mut self, value: f64) {
      if self.ring_buffer.len() == self.ring_buffer.capacity() {
        self.ring_buffer.pop_back();
      }

      self.ring_buffer.push_front(value);
    }

    pub fn reduce(&self, len: usize) -> f64 {
      //assert!(len < self.ring_buffer.capacity(), "cma max len");
      self
        .ring_buffer
        .iter()
        .take(len)
        .fold(0.0, |acc, x| acc + x)
        / len.min(self.ring_buffer.len()) as f64
    }

    pub fn value(&self) -> f64 {
      self.reduce(self.ring_buffer.len())
    }

    pub fn signal(&mut self, value: f64) -> f64 {
      self.acc(value);
      self.reduce(self.ring_buffer.len())
    }
  }
}

pub fn ema(decay: f64) -> ema::State {
  ema::State { decay: decay.recip(), state: 1.0 }
}

pub mod ema {

  pub struct State {
    pub decay: f64,
    pub state: f64,
  }

  impl State {
    pub fn signal(&mut self, value: f64) -> f64 {
      self.state = self.decay * value + (1.0 - self.decay) * self.state;
      self.state
    }

    pub fn value(&self) ->f64 {
      self.state
    }
  }
}


pub fn ma(init: f64) -> ma::State {
  ma::State {
    sum: init,
    count: 1.0,
  }
}

pub mod ma {

  pub struct State { // CMA
    pub sum: f64,
    pub count: f64,
  }

  impl Default for State {
    fn default() -> State {
      State {
        sum: 0.0,
        count: 0.0,
      }
    }
  }

  impl State {
    pub fn acc(&mut self, value: f64) {
      self.sum += value;
      self.count += 1.0;
    }

    pub fn reduce(&mut self) -> f64 {
      let ma = self.sum / self.count;
      self.sum = ma;
      self.count = 1.0;
      ma
    }

    pub fn value(&mut self) -> f64 {
      self.sum / self.count
    }

  }
}
