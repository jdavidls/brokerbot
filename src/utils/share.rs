use crate::prelude::*;

// tools

pub fn share<Item>(mut stream: Pin<Box<Stream<Item = Result<TradeEvent>> + Send + Unpin + 'static>>) -> Sampler
where
  TS: ,
{
  //let mut timer = interval(duration);

  let (tx, rx) = watch::channel(Sample{});

  let handle = spawn(async move {
    loop {
      select! {
        trade = trade_stream.next() => {
          
        },
        tick = timer.next() => {

        },
        else => break
      }
    }
  });

  Sampler { rx, handle }
}

#[pin_project]
#[must_use = "streams do nothing unless polled"]
pub struct Sampler {
  #[pin]
  rx: watch::Receiver<Sample>,
  handle: tokio::task::JoinHandle<()>,
}

impl Stream for Sampler {
  type Item = Sample;

  delegate_stream!(rx);
}
