use crate::prelude::*;

use tokio::sync::broadcast::{channel, Receiver, Sender};

pub fn pump<T>() -> Pump<T>
where
  T: Clone + Send,
{
  let (tx, _) = channel(256);

  Pump { tx, handle: None }
}

pub struct Pump<T> {
  tx: Sender<T>,
  handle: Option<tokio::task::JoinHandle<()>>,
}

impl<T> Pump<T> {
  pub fn start<St>(&mut self, stream: St)
  where
    St: Stream<Item = T> + Send + 'static,
    T: Send + 'static
  {
    let mut stream = stream.boxed();
    let tx = self.tx.clone();

    let handle = tokio::task::spawn(async move {
      //tokio::task::yield_now().await;

      while let Some(item) = stream.next().await {
        if let Err(e) = tx.send(item) {
          panic!("Pumping error");
        }

        //tokio::task::yield_now().await;
      }
    });

    self.handle.replace(handle);
  }

  pub fn stream(&self) -> Receiver<T> {
    self.tx.subscribe()
  }
}
