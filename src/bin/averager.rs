#![recursion_limit = "1024"]
use brokerbot::binance;
use brokerbot::broker;
use brokerbot::prelude::*;
//use brokerbot::settings::*;
use brokerbot::utils::*;

use std::io;
use termion::raw::IntoRawMode;
use tui::backend::TermionBackend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Style};
use tui::text::{Span, Text};
use tui::widgets::{Block, Borders, Gauge, Row, Table, Widget};
use tui::Terminal;

const RING_BUFFER_LEN: usize = 65_536;

const CYCLE_DURATION_MS_u64: u64 = 15_000;
const CYCLE_DURATION_MS_usize: usize = 15_000;


const SETTINGS_FILE: &str = ".config/balancer.toml";


#[derive(Serialize, Deserialize, Default, Debug, Clone)]
pub struct Settings {
  pub broker: broker::Settings,
  //pub probe_lifetime_secs
  //pub max_delfos_probes:
}

struct MarketInfo {
  current_price: f64,
  captured: bool,

  counter: f64,

  avg_price: ma::State,

  price: Ring,  
  delfos: Delfos,
  stats: (f64, f64, f64),

  //attack_l: ema::State,
  //attack_s: ema::State,
  //release_l: ema::State,  
  //release_s: ema::State,

  attack_l: cma::State,
  attack_s: cma::State,
  release_l: cma::State,  
  release_s: cma::State,

}


const MINUTES: f64 = 60_000.0 / CYCLE_DURATION_MS_u64 as f64;

const ATTACK_S_DECAY: f64 =  15.0 * MINUTES;
const ATTACK_L_DECAY: f64 = 120.0 * MINUTES;

const RELEASE_S_DECAY: f64 = ATTACK_S_DECAY;
const RELEASE_L_DECAY: f64 = ATTACK_L_DECAY;

const ATTACK_TIME: f64 = 1.0 * MINUTES;
const RELEASE_TIME: f64 = 2.0 * MINUTES;

const DELFOS_MAX_LIFETIME: f64 = 60.0 * MINUTES;

impl MarketInfo {
  pub fn new(current_price: f64) -> MarketInfo {
    MarketInfo {
      current_price,

      captured: false,
      counter: 0.0,

      avg_price: ma(current_price),

      price: ring(RING_BUFFER_LEN).fill(current_price),
      delfos: delfos(1.001 * 1.001, DELFOS_MAX_LIFETIME as usize), 
      stats: (1.0, 1.0, 1.0),

      attack_l: cma(ATTACK_L_DECAY as usize),
      attack_s: cma(ATTACK_S_DECAY as usize),

      release_l: cma(RELEASE_L_DECAY as usize),
      release_s: cma(RELEASE_S_DECAY as usize),
    }
  }

  pub fn on_trade_event(&mut self, trade: &TradeEvent) {
    self.avg_price.acc(trade.price);
  }

  pub fn on_sync_event(&mut self) {
    self.current_price = self.avg_price.reduce();

    let delta = self.current_price / self.price.last().unwrap();

    self.price.insert(self.current_price);
    self.stats = self.delfos.signal(delta);

    let attack_l = self.attack_l.signal(self.current_price);
    let attack_s = self.attack_s.signal(self.current_price);

    let release_l = self.release_l.signal(self.current_price);
    let release_s = self.release_s.signal(self.current_price);

    if !self.captured { // ATTACK

      if attack_s > attack_l { // && self.stats.2 < 0.15 && self.stats.0 > 1.0 {
        self.counter += 1.0;

        if self.counter >= ATTACK_TIME {
          self.counter = 0.0;
          self.captured = true;
        }

      } else {
        self.counter = (self.counter - 3.0).max(0.0);
      }
    }
    else { // RELEASE
  
      if release_s < release_l { 
        self.counter += 1.0;

        // RELEASE
        if self.counter >= RELEASE_TIME {
          self.counter = 0.0;
          self.captured = false;
        }
      } else {
        self.counter = (self.counter - 3.0).max(0.0);
      }
    }

  }
}

struct Asset {
}

impl Asset {
  pub fn on_sync_event() {}
}

impl Default for Asset {
  fn default() -> Asset {
    Asset {
    }
  }
}

enum Event {
  Broker(broker::Event),
  Refresh(Instant),
}

#[tokio::main]
pub async fn main() -> Result<()> {
  let settings = load_or_default::<Settings>(SETTINGS_FILE).await?;

  println!("Connecting ...");
  let mut broker = settings.broker.connect().await?;

  let mut pairs = std::collections::HashMap::new();
  let mut assets: std::collections::HashMap<Token, Asset> = std::collections::HashMap::new();
  //let mut all_trade_streams = Vec::with_capacity(PAIRS.len());

  for pair in broker.token_pairs().drain(..) {
    pairs.insert(pair, MarketInfo::new(broker.get_price(pair).await?));

    //all_trade_streams.push(stream.map_ok(move |trade| (pair, trade)));

    assets.entry(pair.from).or_default();
    assets.entry(pair.to).or_default();
  }

  let mut refresh_timer = tokio::time::interval(Duration::from_millis(100)).fuse();
/*
  let stdout = io::stdout(); //.into_raw_mode()?;
  let backend = TermionBackend::new(stdout);
  let mut terminal = Terminal::new(backend)?;
  terminal.clear()?;
*/

  //let downstream =
  let mut cycle_open_at: u64 = 0;
  let mut cycles: usize = 0;

  broker.set_balance(USDT, 1_000.0);

  let mut capturing_amount: f64 = 0.0;
  let mut capturing: Option<TokenPair> = None;
  let mut transactions: usize = 0;

  let loss: f64 = 0.0;
  let penalties: f64 = 0.0;
  let gain: f64 = 0.0;
  let target = 1.01_f64;

  let mut stop_loss: f64 = 0.0;

  loop {

    let event = futures::select! {
      e = broker.try_next() => match e {
        Ok(Some(event)) => Event::Broker(event),
        Ok(None) => panic!("Broken connection"),
        Err(e) => return Err(e),
      },
      e = refresh_timer.next() => match e {
        Some(instant) => Event::Refresh(instant),
        None => return Ok(()),
      },
    };

    match event {
      Event::Broker(broker::Event::LiveMode) => {
        
        //broker.set_real_mode().await?;

        //break Ok(()); // solo era una prueba...
      },
      Event::Broker(broker::Event::Trade(ref trade)) => {
        while trade.at > cycle_open_at + CYCLE_DURATION_MS_u64 {
          cycle_open_at = if cycle_open_at == 0 {
            trade.at
          } else {
            cycle_open_at + CYCLE_DURATION_MS_u64
          };

          for (symbol, pair) in pairs.iter_mut() {
            pair.on_sync_event();
          }

          /// CAPTURA!!!
          match capturing {
            None => { // ATTACK
              for (pair, inner) in pairs.iter_mut() {
                if inner.captured {
                  broker.market_order(*pair, Side::Buy, f64::INFINITY).await?;
                  capturing = Some(*pair);
                  transactions += 1;
                  break; //// <<<< XXXX IMPORTA
                }
              }
            },
            Some(pair) => {// RELEASE
              let inner = pairs.get_mut(&pair).unwrap();

              if !inner.captured {
                broker.market_order(pair, Side::Sell, f64::INFINITY).await?;
                capturing = None;
                transactions += 1;
              }
            },
          }
          

          tokio::task::yield_now().await;
          cycles += 1;
        }

        if let Some(pair) = pairs.get_mut(&trade.pair) {
          pair.on_trade_event(&trade);
        }
      }

      Event::Refresh(instant) => {
/*
        terminal.draw(|f| {
          let layout = Layout::default()
            .direction(Direction::Vertical)
            //.vertical_margin(1)
            .margin(1)
            .constraints(
              [
                Constraint::Length(3 + 3 * pairs.len() as u16),
                Constraint::Length(3 + 2 * pairs.len() as u16),
                Constraint::Length(2),
              ]
              .as_ref(),
            )
            .split(f.size());

          f.render_widget(
            Table::new(
              [
                "Market",
                "Price",
                "Attack",
                "Release",
                "Status",
                "Buy",
                "Sell",
                "Doubt"
              ]
              .into_iter(),
              pairs.iter().flat_map(|(symbol, pair)| {
                vec![
                  Row::Data(
                    vec![
                      format!("{} / {}", symbol.to, symbol.from),
                      format!("{:.6}", pair.current_price),
                      format!("{:.6}", pair.attack_s.value()),
                      format!("{:.6}", pair.release_s.value()),
                      format!("{}", if pair.captured {"captured"} else {"released"}),
                      repr_percent(pair.stats.0),
                      repr_percent(pair.stats.1),
                      format!("{:.6}", pair.stats.2),
                    ]
                    .into_iter(),
                  ),
                  Row::Data(
                    vec![
                      format!(""),
                      format!(""),
                      format!("{:.6}", pair.attack_l.value()),
                      format!("{:.6}", pair.release_l.value()),
                      format!("{}", pair.counter),
                      format!(""),
                      format!(""),
                      format!(""),
                    ]
                    .into_iter(),
                  ),
                ]
                .into_iter()
              }),
            )
            .block(Block::default().title("MarketInfos"))
            .header_style(Style::default().fg(Color::Magenta))
            .widths(&[
              Constraint::Length(12),
              Constraint::Length(12),
              Constraint::Length(12),
              Constraint::Length(12),
              Constraint::Length(12),
              Constraint::Length(12),
              Constraint::Length(12),
              Constraint::Length(12),
            ])
            //.style(Style::default().fg(Color::White))
            .column_spacing(1),
            layout[0],
          );

          f.render_widget(
            Table::new(
              ["Asset", "qty"].into_iter(),
              assets.iter().map(|(token, asset)| {
                Row::Data(
                  vec![
                    format!("{}", token),
                    format!("{:.6}", broker.get_balance(*token)),
                  ]
                  .into_iter(),
                )
              }),
            )
            .block(Block::default().title("Assets"))
            .header_style(Style::default().fg(Color::Magenta))
            .widths(&[
              Constraint::Length(8),
              Constraint::Length(12),
            ])
            .column_spacing(1),
            layout[1],
          );

          
          let usdt = broker.get_balance(USDT);

          f.render_widget(
            Gauge::default()
              .block(Block::default().title(format!(
                "cycle: {}, balance: {}, capturing: {:?}, transactions: {}, live_mode: {}, real_mode: {}, ",
                cycles, usdt, capturing, transactions, broker.is_live_mode(), broker.is_real_mode()
              )))
              .gauge_style(Style::default().fg(Color::White).bg(Color::Black))
              .percent((100 * cycles.min(RING_BUFFER_LEN) / RING_BUFFER_LEN) as u16),
            layout[2],
          );
        })?;
*/
        // display block  

      }



    }
  }
}

fn delta_style(v: f64) -> Style {
  if v < 1.0 {
    Style::default().fg(Color::Red)
  } else if v > 1.0 {
    Style::default().fg(Color::Green)
  } else {
    Style::default().fg(Color::Cyan)
  }
}

fn repr_time(seconds: f64) -> String {
  if seconds.is_infinite() {
    return "- --- --- -".to_owned();
  }

  let sign = if seconds.is_sign_negative() {
    '↓'
  } else {
    '↑'
  };

  let mut seconds = seconds.abs();

  let hours = (seconds / 3600.0).floor();
  seconds -= hours * 3600.0;

  let minutes = (seconds / 60.0).floor();
  seconds -= minutes * 60.0;

  if hours > 1.0 {
    format!("{} {:02.0}h {:02.0}m {}", sign, hours, minutes, sign)
  } else {
    format!("{} {:02.0}m {:02.0}s {}", sign, minutes, seconds, sign)
  }
}
fn repr_percent(mut value: f64) -> String {
  format!("{:+2.6}%", 100.0 * (value - 1.0))
}

/*
let green_style = Style::default().fg(Color::Green);
let row_style = Style::default().fg(Color::White);
let blue_style = Style::default().fg(Color::Cyan);
*/

pub async fn load_or_default<T>(path: &str) -> Result<T>
where
  T: Default + serde::Serialize + serde::de::DeserializeOwned,
{
  if let Ok(path) = tokio::fs::canonicalize(path).await {
    let tmp = tokio::fs::read(path).await?;
    toml::from_slice::<T>(&tmp).map_err(|x| x.into())
  } else {
    let data = Default::default();
    tokio::fs::write(path, toml::to_string_pretty(&data)?).await?;
    Ok(data)
  }
}


